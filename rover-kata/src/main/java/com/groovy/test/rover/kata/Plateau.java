/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.groovy.test.rover.kata;

/**
 *
 * @author mdjona
 */
public class Plateau {

    private final Integer maxX,
            maxY,
            minX = 0,
            minY = 0;
    
   
   public Plateau(int maxX, int maxY) {
       this.maxX = maxX;
       this.maxY = maxY;
   }

    public Integer getMaxX() {
        return maxX;
    }

    public Integer getMaxY() {
        return maxY;
    }

    public Integer getMinX() {
        return minX;
    }

    public Integer getMinY() {
        return minY;
    }
}
