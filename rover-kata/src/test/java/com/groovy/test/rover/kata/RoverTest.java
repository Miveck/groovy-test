/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.groovy.test.rover.kata;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author mdjona
 */
public class RoverTest {

    @Test
    public static void roverPositionAndDirectionShouldBeInitiatedWhenItIsCreated() {
        Rover rover = new Rover(0, 0, 'N');

        Assert.assertNotNull(rover.getY());
        Assert.assertNotNull(rover.getX());
        Assert.assertNotNull(rover.getDirection());
    }

    @Test
    public void roverCoordinatesAndDirectionShouldBeWhatWeExpectOnInitialization() {
        Rover rover = new Rover(5, 4, 'N');

        Assert.assertEquals(rover.getX().intValue(), 5);
        Assert.assertEquals(rover.getY().intValue(), 4);

        Assert.assertEquals(rover.getDirection(), 'N');
    }
    
    
}
