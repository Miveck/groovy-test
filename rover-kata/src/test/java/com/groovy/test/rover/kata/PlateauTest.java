/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.groovy.test.rover.kata;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author mdjona
 */
public class PlateauTest {

    @Test
    public static void plateauMaxAndMinCoordinatesShouldBeInitiatedWhenItisCreated() {
        Plateau plateau = new Plateau(5, 5);

        Assert.assertNotNull(plateau.getMaxY());
        Assert.assertNotNull(plateau.getMaxX());
        Assert.assertNotNull(plateau.getMinX());
        Assert.assertNotNull(plateau.getMinY());
    }

    @Test
    public static void plateauMaxAndMinCoordinatesShouldBeTheOnesIHaveInitiatedPlateauWith() {
        // Arrange
        Plateau plateau = new Plateau(5, 5);
        
        // Act
        int maxX = plateau.getMaxX().intValue();
        int maxY = plateau.getMaxY().intValue();
        int minX = plateau.getMinX().intValue();
        int minY = plateau.getMinY().intValue();

        // Assert
        Assert.assertEquals(maxX, 5);
        Assert.assertEquals(maxY, 5);
        Assert.assertEquals(minX, 0);
        Assert.assertEquals(minY, 0);
    }
}
